The "pwd" plug-in supports the functionality to print the working directory. It prints the entire path to the current directory.

Sample Test
-----------
esh> pwd
Using plugin: <Directory path>
esh>

Written By
----------
Jishnu Renugopal
Mattin Zargarpur

