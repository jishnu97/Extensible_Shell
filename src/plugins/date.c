#include "../esh.h"
#include<time.h>
#include<stdio.h>
static bool
init_plugin(struct esh_shell *shell)
{
    printf("Plugin 'date' initialized...\n");
    return true;
}
static bool date(struct esh_command *cmd)
{
        if (strcmp(cmd->argv[0], "date"))
        {
                return false;
        }
	//Intialize a timer variable
	time_t tim = time(NULL);
	//Fills the struct tm with values corresponding to local time
	struct tm *tm = localtime(&tim);
	//Prints the time and date
	printf("Using plugin: %s", asctime(tm));
	return true;
}
struct esh_plugin esh_module = {
  .rank = 3,
  .init = init_plugin,
  .process_builtin = date
};

