#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "../esh.h"
static bool 
init_plugin(struct esh_shell *shell)
{
    printf("Plugin 'pwd' initialized...\n");
    return true;
}
static bool pwd(struct esh_command *cmd)
{
	if (strcmp(cmd->argv[0], "pwd"))
        {
		return false;
	}
	//An array to hold the name of the working directory
	char wd[1024];
	if (getcwd(wd, sizeof(wd)) != NULL)
	{
		//Prints the current working directory
		fprintf(stdout, "Using plugin: %s\n", wd);
	}
	return true;
}
struct esh_plugin esh_module = {
  .rank = 2,
  .init = init_plugin,
  .process_builtin = pwd
};
