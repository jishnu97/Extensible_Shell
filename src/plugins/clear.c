#include <stdio.h>
#include <stdlib.h>
#include <termcap.h>
#include "../esh.h"
static bool
init_plugin(struct esh_shell *shell)
{
    printf("Plugin 'clear' initialized...\n");
    return true;
}
static bool clear(struct esh_command *cmd)
{
	if (strcmp(cmd->argv[0], "clear"))
	{
        return false;
	}
	printf("Using plugin to clear console");
	char buffer[1024];
	char *str;
	//Get the termial type
	tgetent(buffer, getenv("TERM"));
	//Gets the string value for clear
	str = tgetstr("cl", NULL);
	//Clears the terminal
	fputs(str, stdout);
	return true;
}
struct esh_plugin esh_module = {
  .rank = 3,
  .init = init_plugin,
  .process_builtin = clear
};

