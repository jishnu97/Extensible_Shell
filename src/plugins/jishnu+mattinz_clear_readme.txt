The "clear" plug-in supports the functionality to clear the console. 

Sample Test
-----------
esh> clear
Using plugin to clear console:

esh>

Written By
----------
Jishnu Renugopal
Mattin Zargarpur

