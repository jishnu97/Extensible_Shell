The "date" plug-in supports the functionality to print the date to the console. It prints the date in the folowing format:
Fri Oct  7 17:36:08 EDT 2016

Sample Test
-----------
esh> date
Using plugin: Fri Oct  7 17:36:08 EDT 2016
esh> 

Written By
----------
Jishnu Renugopal
Mattin Zargarpur
