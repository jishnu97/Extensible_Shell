/*
 * esh - the 'pluggable' shell.
 *
 * Developed by Godmar Back for CS 3214 Fall 2009
 * Virginia Tech.
 */
#include <stdlib.h>
#include <stdio.h>
#include <readline/readline.h>
#include <unistd.h>
#include <sys/wait.h>
#include<signal.h>
#include "esh.h"
#include<inttypes.h>
#include<setjmp.h>
#include<assert.h>
#include <fcntl.h>
static struct list jobList;
static sigjmp_buf jumpBuf;

static void
usage(char *progname)
{
    printf("Usage: %s -h\n"
        " -h            print this help\n"
        " -p  plugindir directory from which to load plug-ins\n",
        progname);

    exit(EXIT_SUCCESS);
}

/* Build a prompt by assembling fragments from loaded plugins that 
 * implement 'make_prompt.'
 *
 * This function demonstrates how to iterate over all loaded plugins.
 */
static char *
build_prompt_from_plugins(void)
{
    char *prompt = NULL;
    struct list_elem * e = list_begin(&esh_plugin_list);

    for (; e != list_end(&esh_plugin_list); e = list_next(e)) {
        struct esh_plugin *plugin = list_entry(e, struct esh_plugin, elem);

        if (plugin->make_prompt == NULL)
            continue;

        /* append prompt fragment created by plug-in */
        char * p = plugin->make_prompt();
        if (prompt == NULL) {
            prompt = p;
        } else {
            prompt = realloc(prompt, strlen(prompt) + strlen(p) + 1);
            strcat(prompt, p);
            free(p);
        }
    }

    /* default prompt */
    if (prompt == NULL)
        prompt = strdup("esh> ");

    return prompt;
}

/* The shell object plugins use.
 * Some methods are set to defaults.
 */
struct esh_shell shell =
{
    .build_prompt = build_prompt_from_plugins,
    .readline = readline,       /* GNU readline(3) */ 
    .parse_command_line = esh_parse_command_line /* Default parser */
};

/**
 * Assign ownership of ther terminal to process group
 * pgrp, restoring its terminal state if provided.
 *
 * Before printing a new prompt, the shell should
 * invoke this function with its own process group
 * id (obtained on startup via getpgrp()) and a
 * sane terminal state (obtained on startup via
 * esh_sys_tty_init()).
 */
static void give_terminal_to(pid_t pgrp, struct termios *pg_tty_state)
{
    esh_signal_block(SIGTTOU);
    int rc = tcsetpgrp(esh_sys_tty_getfd(), pgrp);
    if (rc == -1)
        esh_sys_fatal_error("tcsetpgrp: ");

    if (pg_tty_state)
        esh_sys_tty_restore(pg_tty_state);
    esh_signal_unblock(SIGTTOU);
}

int
main(int ac, char *av[])
{
	list_init(&jobList);
	esh_signal_sethandler(SIGCHLD, sigchildHandler);
	esh_signal_sethandler(SIGTSTP, sigstopHandler);
	esh_signal_sethandler(SIGINT, sigintHandler);
    int opt;
    list_init(&esh_plugin_list);

    /* Process command-line arguments. See getopt(3) */
    while ((opt = getopt(ac, av, "hp:")) > 0) {
        switch (opt) {
        case 'h':
            usage(av[0]);
            break;

        case 'p':
            esh_plugin_load_from_directory(optarg);
            break;
        }
    }

    esh_plugin_initialize(&shell);

	setpgid(0,0);
	struct termios* shellTerm = esh_sys_tty_init();

	//Sets jump point for when ^C is pressed.
	//May not be properly placed. May cause mem leaks.
	if(sigsetjmp(jumpBuf, 1))
	{
		printf("\n");
	}
    /* Read/eval loop. */
    for (;;) {
	give_terminal_to(getpgrp(), shellTerm);

        /* Do not output a prompt unless shell's stdin is a terminal */
        char * prompt = isatty(0) ? shell.build_prompt() : NULL;
        char * cmdline = shell.readline(prompt);
        free (prompt);

        if (cmdline == NULL)  /* User typed EOF */
            break;

        struct esh_command_line * cline = shell.parse_command_line(cmdline);
        free (cmdline);
        if (cline == NULL)                  /* Error in command line */
            continue;

        if (list_empty(&cline->pipes)) {    /* User hit enter */
            esh_command_line_free(cline);
            continue;
        }


	struct list_elem* e;
	for(e=list_begin(&(cline->pipes));e!=list_end(&(cline->pipes));e=list_next(e))
	{
		struct esh_pipeline* pipeLine = list_entry(e, struct esh_pipeline, elem);
		struct list_elem* commandElem = list_begin(&pipeLine->commands);
		struct esh_command* firstCommand = list_entry(commandElem, struct esh_command, elem);
		int commandType = getCommandType(firstCommand);
		if(commandType >= 0)
		{
			int arg = (firstCommand->argv)[1] == NULL ? 0 :atoi((firstCommand->argv)[1]);
			//Runs the appropriate built-in
			switch(commandType)
			{
				case 0:
					exit(EXIT_SUCCESS);
				case 1:
					bi_jobs();
					break;
				case 2:
					if(arg > 0)
					{
						bi_fg(arg, shellTerm);
					}
					break;
				case 3:
					if(arg > 0)
					{
						bi_bg(arg);
					}
					break;
				case 4:
					if(arg > 0)
					{
						bi_kill(arg);
					}
					break;
				case 5:
					if(arg > 0)
                     {
                          bi_stop(arg);
                     }
                    break;
			}
		}
		else if(handlePlugins(pipeLine)!=1)
		{
			//If the command is neither a built-in, nor a plugin, 
			//it is run as an external command
			list_pop_front(&cline->pipes);
			handleExternalCommands(pipeLine, shellTerm);
		}
	}

        esh_command_line_free(cline);
    }
    return 0;
}

/* This function runs jobs when they are plugins*/
int handlePlugins(struct esh_pipeline* pipeLine)
{
 
  struct list_elem * p = list_begin(&esh_plugin_list);
  struct list_elem *e = list_begin(&pipeLine->commands);
  struct esh_command *com = list_entry(e, struct esh_command, elem);
  int flag = 0;
  for (; p != list_end(&esh_plugin_list); p = list_next(p))
    {
        struct esh_plugin *plugin = list_entry(p, struct esh_plugin, elem);
        // Checks if the command is a plugin and runs it
        if (plugin->process_builtin && plugin->process_builtin(com))
        {
			flag = 1;
        }
    }
  // If a plugin was executed, 1 is returned  
  return flag;
}

/* Creates a running job for pipeLine */
void handleExternalCommands(struct esh_pipeline* pipeLine, struct termios* shellTerm)
{
	int pipe1[2];
	int pipe2[2];
	esh_signal_block(SIGCHLD);
	int numCommands = (int)list_size(&pipeLine->commands);
	pid_t childPids[numCommands];

	int currentCommand = 0;
	struct list_elem* commandElem;
	for(commandElem = list_begin(&pipeLine->commands);commandElem != list_tail(&pipeLine->commands);commandElem = list_next(commandElem))
	{
		struct esh_command* command = list_entry(commandElem, struct esh_command, elem);
		pid_t childPid;
		if(shouldPipeline(pipeLine)&& list_next(commandElem)!=list_tail(&pipeLine->commands))
        {
			//Create a pipe if required
			pipe(pipe2);
        }
		if((childPid = fork()) == 0)//Fork off a child
		{
			//Set process group
			if(currentCommand == 0)
			{
				setpgid(getpid(), 0);
			}
			else
			{
				setpgid(getpid(), childPids[0]);
			}

			if(shouldPipeline(pipeLine)&& commandElem!=list_begin(&pipeLine->commands))
			{
				//Close the appropriate ends and connect using dup2
				close(pipe1[1]);
				dup2(pipe1[0],0);
				close(pipe1[0]);
			}		
            if(shouldPipeline(pipeLine)&& list_next(commandElem)!=list_tail(&pipeLine->commands))
			{
				//Close the appropriate ends and connect using dup2
                close(pipe2[0]);
                dup2(pipe2[1],1);
                close(pipe2[1]);
             }
			handleIO(command);
			execvp(command->argv[0], command->argv);
		}
		if(childPid < 0) 
        {
             perror("fork failed"), exit(-1);
        }

		//As parent, record PID and advance loop.
		childPids[currentCommand] = childPid;
		command->pid = childPid;
		currentCommand++;
		if(shouldPipeline(pipeLine)&& commandElem!=list_begin(&pipeLine->commands))
        {
                close(pipe1[0]);
                close(pipe1[1]);
        }
		if(shouldPipeline(pipeLine) && list_next(commandElem)!=list_tail(&pipeLine->commands))
        {
				pipe1[0] = pipe2[0];
				pipe1[1] = pipe2[1];
        }
		if(shouldPipeline(pipeLine)&& (list_next(commandElem)==list_tail(&pipeLine->commands)))
        {
				//Close all the pipes once the end of the commands list is reached
				close(pipe1[0]);
				close(pipe1[1]);
				close(pipe2[0]);
				close(pipe2[1]);
        }
		
	}

	int pidIdx;
	setpgid(childPids[0], 0);
	//Set the process group IDs
	for(pidIdx = 1; pidIdx < numCommands; pidIdx++)
	{
		setpgid(childPids[pidIdx], childPids[0]);	
	}
	pipeLine->pgrp = childPids[0];
	//Set the job ID and add the pipeline to the jobs list
	static int jobId = 1;
	if(list_size(&jobList)<=0)
	{
		jobId = 1;
	}
	pipeLine->jid = jobId;
	pipeLine->status = pipeLine->bg_job ? BACKGROUND : FOREGROUND;
	jobId++;
	addJob(pipeLine);

	if(!pipeLine->bg_job)
	{
		give_terminal_to(pipeLine->pgrp, shellTerm);
		waitOnJob(pipeLine);
	}
	else
	{
		printf("[%d] %d\n", pipeLine->jid, (int)pipeLine->pgrp);	
	}
	esh_signal_unblock(SIGCHLD);
}

/* Handles IO redirection */
void handleIO(struct esh_command* command)
{
	//Redirects input
	if(command->iored_input!=0)
	{
		int in = open(command->iored_input, O_RDONLY);
		if(dup2(in, 0)<0)
	        {
				fprintf(stderr, "Error dup2");
	        }
	        close(in);
	}
	//Redirects output
	if(command->iored_output !=0)
	{
		int out;
	        if(!command->append_to_output)
	        {
				//Overwrites to out
				out = open(command->iored_output,    O_WRONLY | O_TRUNC | O_CREAT, 
							S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
	        }
	        else  
	        {
				//Appends to out
				out = open(command->iored_output,O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
	        }
	        if(dup2(out,1)<0)
	        {
				fprintf(stderr,"Error dup2");
        	}
	        close(out);
	}
}

/* Returns true if pipe will require pipes. */
bool shouldPipeline(struct esh_pipeline * pipe)
{
	return list_size(&pipe->commands)>1;
}

/* Makes the shell wait until the job represented by pipeLine completes or moves into the background */
void waitOnJob(struct esh_pipeline* pipeLine)
{
	assert(esh_signal_is_blocked(SIGCHLD));
	//Until the foreground job terminates, this esentially becomes the signal handler.
	while(pipeLine->status == FOREGROUND && !list_empty(&pipeLine->commands))
	{
		int  status = 0;
		pid_t pid = waitpid(-1, &status, WUNTRACED);
		if(pid != -1)
		{
			handleChildStatusChange(pid, status);
		}
	}
}

/* Removes the esh_command that corresponds to commandPid from pipeLine */
void removeCommandFromPipeline(pid_t commandPid, struct esh_pipeline* pipeLine)
{
	struct list_elem *e;
        for (e = list_begin (&pipeLine->commands); e != list_end (&pipeLine->commands);
           e = list_next (e))
	{
		struct esh_command* command = list_entry(e, struct esh_command, elem);
		if(command->pid == commandPid)
		{
			list_remove(e);
			return;
		}
	}
}

/* Updates the esh_pipeline to which pid belongs */
void handleChildStatusChange(pid_t pid, int status)
{
	struct esh_pipeline* pipeLine = get_job_from_pid(pid);
	if(pipeLine == 0)
	{
		return;
	}

	if(WIFEXITED(status) || WIFSIGNALED(status))
	{
		//Removes the command if it has terminated
		if(!list_empty(&pipeLine->commands))
		{
			removeCommandFromPipeline(pid, pipeLine);
		}
		//Removes the job if the entire pipeline has terminated
		if(list_empty(&pipeLine->commands))
		{
			removeJob(pipeLine->jid);
		}
	}
	else if(WIFSTOPPED(status))
	{
		//Saves the state if the pipeline has been stopped
		esh_sys_tty_save(&pipeLine->saved_tty_state);
		bi_stop(pipeLine->jid);
	}
}



/* Returns an integer signifying the type of the built in function*/
int getCommandType(struct esh_command* command)
{
        int result = -1;
        char* commandStr = (command->argv)[0];
        if(strcmp(commandStr, "exit") == 0)
        {
                result = 0;
        }
        else if(strcmp(commandStr, "jobs") == 0)
        {
                result = 1;
        }
        else if(strcmp(commandStr, "fg") == 0)
        {
                result = 2;
        }
        else if(strcmp(commandStr, "bg") == 0)
        {
                result = 3;
        }
        else if(strcmp(commandStr, "kill") == 0)
        {
                result = 4;
        }
        else if(strcmp(commandStr, "stop") == 0)
        {
                result = 5;
        }

        return result;
}

/* Prints out the jobs list to stdout */
void bi_jobs()
{
	if(list_empty(&jobList))
	{
		return;
	}
	struct list_elem *e;
	//This loop iterated through the jobs list printing out each job
	for (e = list_begin (&jobList); e != list_end (&jobList); e = list_next (e))
	{
		struct esh_pipeline *job = list_entry (e, struct esh_pipeline, elemJList);
		printJob(job);
	}
}

/* Sends the job with the given ID to the foreground*/
void bi_fg(int jobID, struct termios* shellTerm)
{
	esh_signal_block(SIGCHLD);
	struct esh_pipeline* pipeLine = get_job_from_jid(jobID);
	if(pipeLine != NULL)
	{
		//Sets the job status and prints the job
		pipeLine->status = FOREGROUND;
		printJobName(pipeLine);
		printf("\n");
		// The terminal is given to the job
		give_terminal_to(pipeLine->pgrp, shellTerm);
		if(kill(pipeLine->pgrp, SIGCONT) < 0)
		{
			esh_sys_fatal_error("fg error");
		}
        waitOnJob(pipeLine);
	}
	esh_signal_unblock(SIGCHLD);
}

/* Sends the job with the given ID to the background*/
void bi_bg(int jobID)
{
	struct esh_pipeline* pipeLine = get_job_from_jid(jobID);
	if(pipeLine != NULL)
	{
		//Sets the status of the job and moves it to the background
		pipeLine->status = BACKGROUND;
		kill(pipeLine->pgrp, SIGCONT);
	}
}

/* Kills the job with the given ID*/
void bi_kill(int jobID)
{
	struct esh_pipeline* pipe = get_job_from_jid(jobID);
	if(pipe!=NULL)
	{
		//Kills the job and removes it from the jobs list
		esh_signal_block(SIGCHLD);
		kill(-pipe->pgrp,SIGKILL);
		//Prints the job ID of the job that was killed
		printf("[%d]\t Killed\n",pipe->jid);
		removeJob(jobID);
		esh_signal_unblock(SIGCHLD);
	}
}

/* Stops the job with the given ID*/
void bi_stop(int jobID)
{
	struct esh_pipeline* pipe = get_job_from_jid(jobID);
        if(pipe!=NULL)
        {
                kill(-pipe->pgrp,SIGSTOP);	
		pipe->status = STOPPED;
                printf("\n");
                printJob(pipe);
        }
}

/*Prints the status of the job */
void printStatus(enum job_status stat)
{
	if(stat==FOREGROUND)
	{
		printf("Foreground");
	}
	if(stat==BACKGROUND)
	{
		printf("Running");
	}
	if(stat==STOPPED)
	{
		printf("Stopped");
	}
	if(stat==NEEDSTERMINAL)
	{
		printf("Needs Terminal");
	}
}

/* Prints one individual job*/
void printJob(struct esh_pipeline* job)
{
	printf("[%d]\t",job->jid);
	printStatus(job->status);
	printf("\t\t\t(");
	printJobName(job);
	printf(")\n");	       
}


/*Prints the job name*/
void printJobName(struct esh_pipeline* job)
{
	struct list_elem *e;
	for (e = list_begin (&job->commands); 
		   e != list_end (&job->commands);
           e = list_next (e))
        {
          struct esh_command *command = list_entry (e, struct esh_command, elem);
          char **pos = (command->argv);
                  while (*pos != '\0')
                  {
						//Prints out onr command with it's argument
                        printf("%s ", *(pos++));
                  }
				  //The proper IO redirection operator is printed
                  if(command->append_to_output)
                  {
                          printf(" >> %s",command->iored_output);
                  }
                  else if(command->iored_output!=NULL)
                  {
                          printf(" > %s",command->iored_output);
                  }
                  else if(command->iored_input!=NULL)
                  {
                          printf(" < %s",command->iored_input);
                  }
				  //If the pipeline has commands that are piped, the 
				  // pipe operator is printed
                  else if(1 < list_size(&job->commands) && 
				  list_next(e)!=list_end(&job->commands))
                  {
                          printf(" | ");
                  }
        }
}

/*Returns the pipeline given the process group*/
struct esh_pipeline* get_job_from_pgrp(pid_t pgid)
{
	struct list_elem *e;
        for (e = list_begin (&jobList); e != list_end (&jobList);
           e = list_next (e))
        {
          struct esh_pipeline *pipe = list_entry (e, struct esh_pipeline, elemJList);
		  if(pipe->pgrp==pgid)
		  {
			//Returns the pipeline that matches the given process group
			return pipe;
		  }
	}
	return 0;
}

/*Returns the pipeline given the job ID*/
struct esh_pipeline* get_job_from_jid(int jobID)
{
        struct list_elem *e;
		//This loop iterates through the jobs list to find the pipeline
        for (e = list_begin (&jobList); e != list_end (&jobList);
           e = list_next (e))
        {
          struct esh_pipeline *pipe = list_entry (e, struct esh_pipeline, elemJList);
         		if(pipe->jid==jobID)
                {
						//Returns the pipeline once it is found
                        return pipe;
                }
        }
        return 0;
}

/*Returns the pipeline containing the command with the given pid*/
struct esh_pipeline* get_job_from_pid(pid_t pid)
{
	struct list_elem *e;
	//These two nested loops iterate through the jobs list and through
	//the commands in each pipeline to find the command with the given 
	//pid
	for(e = list_begin(&jobList); e != list_end(&jobList); e = list_next(e))
	{
		struct esh_pipeline *pipe = list_entry (e, struct esh_pipeline, elemJList);
		struct list_elem* commandElem;
		for(commandElem = list_begin(&pipe->commands); 
			commandElem != list_end(&pipe->commands); 
			commandElem = list_next(commandElem))
		{
			struct esh_command* command = list_entry(commandElem, struct esh_command, elem);
			if(command->pid == pid)
			{
				//Returns the pipeline once the given pid is found
				return pipe;
			}
		}
	}
	return NULL;
}

/*Removes the job with the given ID from the jobs list*/
struct esh_pipeline* removeJob(int jobID)
{
		struct esh_pipeline* remJob = get_job_from_jid(jobID);
		list_remove(&(get_job_from_jid(jobID)->elemJList));
		//Remove the job and returns it
		return 	remJob;
}

/*Adds the job to the jobs list*/
void addJob(struct esh_pipeline* job)
{
		//Adds the job too the jobs list
		list_push_front(&jobList, &(job->elemJList));
}

/* Returns the command associated with the pid*/
struct esh_command* get_cmd_from_pid(pid_t pid)
{
	struct list_elem *ePipeline;
	struct list_elem *eCommand;
    for (ePipeline = list_begin (&jobList); ePipeline != list_end (&jobList);
           ePipeline = list_next (ePipeline)) //Iterates through the 
											  // Jobs list
		{	
          struct esh_pipeline *pipe = list_entry (ePipeline, struct esh_pipeline, elemJList);
		  for (eCommand = list_begin (&pipe->commands); eCommand != list_end (&pipe->commands); 
			eCommand = list_next (eCommand))
        		{
         		 struct esh_command *command = list_entry (eCommand, struct esh_command, elem);
				 printf("%s",command->argv[0]);	
				 if(command->pid==pid)
				 {
					//Returns the command struct once it is found
					return command;
				 }
			    }
        }
	//Returns null if the command does not exist
	return NULL;
}

/*Signal handler for SIGCHLD*/
void sigchildHandler(int sig, siginfo_t* sigInfo, void* notSure)
{
	int status = 0;
	pid_t pid;
	while((pid = waitpid(-1, &status, WUNTRACED|WNOHANG)) > 0)
	{
		handleChildStatusChange(pid, status);
	}
}

/*Signal handler for SIGSTOP*/
void sigstopHandler(int sig, siginfo_t* sigInfo, void* notSure)
{
        //Do nothing. Prevents shell from being put to sleep.
}

/*Signal handler for SIGINT*/
void sigintHandler(int sig, siginfo_t* sigInfo, void* notSure)
{
	siglongjmp(jumpBuf, 1);
}
