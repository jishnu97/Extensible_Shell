Student Information
-------------------
Jishnu Renugopal, jishnu
Mattin Zargarpur, mattinz

How to execute the shell
------------------------
1. From the src folder, use the makefile to build the executable.
2. To run the shell without any plugins, type ./esh.
3. To run the shell with plugins, type ./esh -p <directory of plugins>. Note that the directory should contain the shared object files for the plugins.

Important Notes
---------------
None

Description of Base Functionality
----------------------------------
Jobs:
This command will iterate through the jobs list and print each job to stdout in the following format:

[Job ID]  Job Status                 (Name of pipeline)

Fg:
This command will retrieve the specified job from the jobs list, set its job status, signal it to run, and give it control of the terminal. The name of the job is printed to stdout.

Bg:
This command will retrieve the specified job from the jobs, set its status, and signal it to run.

Kill:
This command will retrieve the specified job from the jobs list, signal it to exit, and remove it from the jobs list. "[x] Killed" is printed to stdout, where x is the job number.

Stop:
This command will retrieve the specified job from the jobs list and signal it to stop. The contents of the specified job is printed in the following format:

[Job ID]  Job Status                 (Name of pipeline)

Ctrl-C:
When there is not a foreground job running, ^C is caught by a signal handler that causes the shell to print a new prompt. When there is a foreground job, ^C is handled by the individual processes of the job.

Ctrl-Z:
When there is not a foreground job running, ^Z is caught by a signal handler and ignored. When there is a foreground job, ^Z is handled by the individual processes of the job.

Description of Extended Functionality
-------------------------------------
Exclusive Access:
The shell is capable of providing exclusive terminal access to programs. This is done by giving control of the terminal to the foreground job's process group. When the foreground job stops, exits, or moves to the background, terminal control is given back to the shell.

IO Redirection:
The shell is capable of successfully appending and overwriting output to files and send input to commands from files. This was done by making use the open function and by passing in the appropriate flags. The file descriptors were redirected from stdout or stdin to the appropriate file.

Piping: 
The shell is capable of successfully pipe both single and multi pipes. It creates pipe, connects the appropriate processes and classes the unnecessary ends. This allows communication between processes.

Implemented Plug-Ins
--------------------
The plugins that we implemented are as follows:

PWD:
The "pwd" plug-in supports the functionality to print the working directory. It prints the entire path to the current directory.
Example: 
esh> Using plugin: Directory path

Date: 
The "date" plug-in supports the functionality to print the date to the console. It prints the date in the folowing format:
Using plugin: Fri Oct  7 17:36:08 EDT 2016

Clear: 
The "clear" plug-in supports the functionality to clear the console.
Example: 
esh> clear
Using plugin to clear console:


The following are the plugins that can be run successfully with our shell:
Group name: Plugin name

1. aglasson+ishitag: perm
2. tims+tommy085: f-to-c
3. conorg95+szuzzah: converter
4. conorg95+szuzzah: platecounter
5. ajw222+lbk96: dyslexia
6. ajw222+lbk96: magic_8_ball
7. akhilg96+abhising: add
8. akhilg96+abhising: subtract
9. akhilg96+abhising: multi
10. marcusw+tianyu: echo
11. rjakec21+hosnyn11: modulo
12. rjakec21+hosnyn11: power
13. matthb7+tttran: harambe
14. matthb7+tttran: numero
15. rohanr+jacob6: deconv
16. rohanr+jacob6: palindrome
17. hanuelee+nnguyen9: ascii
18. hanuelee+nnguyen9: tip
19. hanuelee+nnguyen9: toromnum
20. meghankh+jodieb: bmi
21. meghankh+jodieb: dfi
22. meghankh+jodieb: fibo
23. meghankh+jodieb: pali
24. meghankh+jodieb: rvst
